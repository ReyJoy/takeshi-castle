using System;
using UnityEngine;

public enum InputType { Keyboard, Mobile }

[RequireComponent(typeof(CarController))]
public class CarUserControl : MonoBehaviour
{
    CarController controller;
    private float h;
    private float v;
    
    public float Horizontal => h;
    public float Vertical => v;
    

    void Awake()
    {
        controller = GetComponent<CarController>();
    }

    void FixedUpdate()
    {
        h = Input.GetAxis("Horizontal-Move");
        v = Input.GetAxis("Vertical-Move");
        float handbrake = Input.GetAxis("Jump");

        /*switch (inputType)
        {
            case InputType.Keyboard:

                h = Input.GetAxis("Horizontal");
                v = Input.GetAxis("Vertical");
                handbrake = Input.GetAxis("Jump");

                break;

            case InputType.Mobile:

                h = CrossPlatformInputManager.GetAxis("Horizontal");
                v = CrossPlatformInputManager.GetAxis("Vertical");
                handbrake = CrossPlatformInputManager.GetAxis("Jump");

                break;
        }*/

        controller.Move(h, v, v, handbrake);
        //controller.Move(h, v, v, handbrake);
    }
}
