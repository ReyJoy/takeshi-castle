﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float multiplier = 1;
    public bool rotate = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Rotate()
    {
        if (!rotate)
        {
            transform.DORotate(new Vector3(0, 30f * multiplier, 0), 0.1f).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
            rotate = true;
        }
        
    }
}
