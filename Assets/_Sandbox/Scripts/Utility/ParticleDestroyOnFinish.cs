﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyOnFinish : MonoBehaviour
{
    private ParticleSystem particle;

    private void Awake()
    {
        particle = GetComponent<ParticleSystem>();
    }

    void Start()
    {
        var duration = particle.main.duration + particle.main.startLifetimeMultiplier;
        Destroy(gameObject, duration);
    }
}
