﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (T) FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogWarning($"Multiple instances of {instance.gameObject.name} detected in scene.");
                        return instance;
                    }

                    if (instance == null)
                    {
                        GameObject singleton = new GameObject();
                        instance = singleton.AddComponent<T>();
                        singleton.name = $"{typeof(T)} (Singleton)";

                        Singleton<T> component = instance.GetComponent<Singleton<T>>();

                        if (component.ShouldNotDestroyOnLoad())
                        {
                            DontDestroyOnLoad(singleton);
                        }
                        
                        Debug.LogWarning($"[Singleton] Creating an instance of {typeof(T)} with DontDestroyOnLoad {component.ShouldNotDestroyOnLoad()}");
                    }
                    else
                    {
                        Debug.LogWarning($"[Singleton] Using instance that already created {instance.gameObject.name}");
                    }
                }
                
                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (instance == null)
            {
                //If I am the first instance, make me the Singleton
                instance = this as T;
            }
            else
            {
                //If a Singleton already exists and you find
                //another reference in scene, destroy it!
                if(this != instance)
                {
                    Destroy(gameObject);
                }
            }
        }

        protected void WakeUp()
        {
            return;
        }

        protected virtual bool ShouldNotDestroyOnLoad()
        {
            return true;
        }

        private void OnDestroy()
        {
            instance = null;
        }
    }