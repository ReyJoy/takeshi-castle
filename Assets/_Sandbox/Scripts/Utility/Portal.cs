﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Portal linkedPortal;

    public bool isActive = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.tag == "Player" || other.tag == "Boomerang") && isActive)
        {
            linkedPortal.Reset();
            other.transform.position = linkedPortal.transform.position;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if ((other.tag == "Player" || other.tag == "Boomerang") && isActive)
        {
            isActive = true;
        }
    }

    public void Reset()
    {
        isActive = false;
        Invoke("Reactive", 0.5f);
    }

    void Reactive()
    {
        isActive = true;
    }
}
