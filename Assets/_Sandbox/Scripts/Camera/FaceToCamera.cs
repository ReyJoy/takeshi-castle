﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceToCamera : MonoBehaviour
{
    private Transform camera;

    private void Awake()
    {
        camera = Camera.main.transform;
    }

    void Start()
    {
        
    }

    public void Update()
    {
        transform.LookAt(transform.position + camera.rotation * Vector3.forward, camera.rotation * Vector3.up);
    }
}
