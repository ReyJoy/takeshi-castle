﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityAtoms;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManager : Singleton<GameManager>
{
    public GameObjectList playerList;
    public Color[] playerColors;
    public GameObject[] boomerangs;
    public GameObject[] boomerangModel;
    public Mesh[] vehicles;
    public Mesh[] characters;
    public Vector3[] spawnPoint;
    public int[] Kills;
    public GameObject[] powerups;
    public Transform[] powerupSpawnPoint;
    public float powerUpInterval = 10f;
    
    public TextMeshProUGUI player1Text;
    public TextMeshProUGUI player2Text;
    public TextMeshProUGUI player3Text;
    public TextMeshProUGUI player4Text;

    protected override void Awake()
    {
        base.Awake();
        playerList.List.Add(null);
        playerList.List.Add(null);
        playerList.List.Add(null);
        playerList.List.Add(null);
    }

    private void Start()
    {
        InvokeRepeating("SpawnPowerup", powerUpInterval, powerUpInterval);
    }


    public void OnPlayerJoined(UnityEngine.InputSystem.PlayerInput value)
    {
        /*
        var players = FindObjectsOfType<Player>();
        foreach (var player in players)
        {
            playerList.Add(player.gameObject);
        }*/
    }

    private void OnApplicationQuit()
    {
        playerList.Clear();
    }

    private void Update()
    {
        player1Text.text = $"Player 1 : {Kills[0]} Kills";
        player2Text.text = $"Player 2 : {Kills[1]} Kills";
        player3Text.text = $"Player 3 : {Kills[2]} Kills";
        player4Text.text = $"Player 4 : {Kills[3]} Kills";
    }

    private void SpawnPowerup()
    {
        var rPow = Random.Range(0, powerups.Length);
        var powerup = powerups[rPow];

        var rLoc = Random.Range(0, powerupSpawnPoint.Length);
        var loc = powerupSpawnPoint[rLoc];

        Instantiate(powerup, loc.position, Quaternion.identity);
    }
}
