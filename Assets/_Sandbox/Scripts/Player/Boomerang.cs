﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Boomerang : MonoBehaviour
{
    public float force = 15f;
    public float magnitudeThreshold = 10f;
    public float comebackDelay = 1f;
    public TrailRenderer trail;
    public GameObject impactParticle;
    public GameObject[] meshes;

    private Action afterComeback;
    private Transform backPoint;
    private Rigidbody rb;
    private bool canComeback = false;
    
    private int owner;
    public int Owner => owner;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void SpawnBoomerang(int owner, Transform backPoint)
    {
        this.backPoint = backPoint;
        this.owner = owner;
        canComeback = false;

        trail.startColor = GameManager.Instance.playerColors[owner];
        trail.endColor = GameManager.Instance.playerColors[owner];
        foreach (var mesh in meshes)
        {
            mesh.GetComponent<Renderer>().material.color = GameManager.Instance.playerColors[owner];
        }

        rb.AddForce(transform.forward * (force * 100f));
        transform.DORotate(new Vector3(0, 60f, 0), 0.1f).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        Invoke("CanComeback", comebackDelay);
    }

    void Update()
    {
        if (rb.velocity.magnitude < magnitudeThreshold && canComeback && backPoint)
        {
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            transform.DOMove(backPoint.position, magnitudeThreshold).SetSpeedBased(true).SetEase(Ease.Linear);
        }
    }

    private void CanComeback()
    {
        canComeback = true;
    }

    public void Kill()
    {
        DOTween.KillAll();
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        Instantiate(impactParticle, other.GetContact(0).point, Quaternion.identity);
    }
}
