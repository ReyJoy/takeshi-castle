﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityAtoms;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public SkinnedMeshRenderer charMesh;
    public MeshFilter vehicleMesh;
    public Animator anim;
    public int playerNumber = 1;
    public GameObjectList playerList;
    public Transform characterModel;
    public GameObject boomerang;
    public Transform spawnPoint;
    public GameObject deathFx;
    public float magnitudeThresholdObstacle = 7f;
    public TextMeshProUGUI playerText;
    public Transform hand;
    public GameObject aimIndicator;
    public GameObject shield;
    public GameObject fireTrail;
    
    private bool canGetBoomerang = false;
    private bool aiming = false;
    private bool hasBoomerang = true;
    private bool hasShield = false;
    private bool hasBoost = false;
    
    private Boomerang currentBoomerang; 
    private CarController carController;
    private Rigidbody rb;
    private Transform camera;
    private Rotator modelBoomerang;
    
    private float hMove;
    private float vMove;
    private float hAim;
    private float vAim;
    private float tmpTorque;
    private float tmpDownForce;
    private float tmpTopSpeed;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        carController = GetComponent<CarController>();
        camera = Camera.main.transform;
        aimIndicator.SetActive(false);
        AssignPlayerNumber();
        
        tmpTorque = carController.m_FullTorqueOverAllWheels;
        tmpDownForce = carController.m_Downforce;
        tmpTopSpeed = carController.m_Topspeed;
    }

    void AssignPlayerNumber()
    {
        for (int i = 0; i < playerList.Count; i++)
        {
            if (!playerList[i])
            {
                playerNumber = i+1;
                playerList[i] = this.gameObject;
                boomerang = GameManager.Instance.boomerangs[i];
                modelBoomerang = Instantiate(GameManager.Instance.boomerangModel[i], hand).GetComponent<Rotator>();
                vehicleMesh.mesh = GameManager.Instance.vehicles[i];
                charMesh.sharedMesh = GameManager.Instance.characters[i];
                transform.position = GameManager.Instance.spawnPoint[i];
                break;
            }
        }
        
        name = $"Player {playerNumber}";
        playerText.text = $"P{playerNumber}";
        playerText.color = GameManager.Instance.playerColors[playerNumber];
        aimIndicator.GetComponent<Renderer>().material.color = GameManager.Instance.playerColors[playerNumber];
    }

    #region Collision

    private void OnTriggerEnter(Collider other)
    {
        var boomerang = other.GetComponent<Boomerang>();
        if (boomerang != null)
        {
            if (canGetBoomerang && boomerang.Owner == playerNumber)
            {
                hasBoomerang = true;
                modelBoomerang.gameObject.SetActive(true);
                boomerang.Kill();
            }
            else if((boomerang.Owner != playerNumber) && !hasShield)
            {
                GameManager.Instance.Kills[boomerang.Owner - 1] += 1;
                Dead();
            }
        }
        
        if (other.tag == "Obstacle")
        {
            if (rb.velocity.magnitude > magnitudeThresholdObstacle)
            {
                Destroy(other.gameObject);
            }
        }

        if (other.tag == "Player")
        {
            if (hasBoost)
            {
                GameManager.Instance.Kills[playerNumber - 1] += 1;
                other.GetComponent<Player>().Dead();
            }
        }
        
    }

    private void Dead()
    {
        //playerList.Remove(gameObject);
        Instantiate(deathFx, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    #endregion

    #region Car Movement

    private void FixedUpdate()
    {
        carController.Move(hMove, vMove, vMove, 0);
    }
    
    public void OnMovement(InputAction.CallbackContext value)
    {
        var val = value.ReadValue<Vector2>();
        hMove = val.x;
        vMove = val.y;
    }

    #endregion

    #region Aiming

    void Update ()
    {
        var dir = new Vector2(hAim, vAim);

        if (dir.x == 0.0 && dir.y == 0.0)
        {
            if (aiming && hasBoomerang)
            {
                ShootBoomerang();
            }
            
            aiming = false;
            anim.SetBool("Aim", false);
        }
        else
        {
            var angle = Mathf.Atan2(-dir.y, dir.x) * Mathf.Rad2Deg;
            characterModel.transform.rotation = Quaternion.Euler(0f, angle - 90 + camera.rotation.eulerAngles.x, 0f);
            aiming = true;
            if (hasBoomerang)
            {
                anim.SetBool("Aim", true);
                modelBoomerang.Rotate();
                aimIndicator.SetActive(true);
            }
            
        }
    }
    
    public void OnAim(InputAction.CallbackContext value)
    {
        var val = value.ReadValue<Vector2>();
        hAim = val.x;
        vAim = val.y;
    }

    #endregion

    #region Shooting

    private void ShootBoomerang()
    {
        currentBoomerang = Instantiate(boomerang, spawnPoint.position, characterModel.transform.rotation).GetComponent<Boomerang>();
        currentBoomerang.SpawnBoomerang(playerNumber, spawnPoint);
        hasBoomerang = false;
        canGetBoomerang = false;
        Invoke("CanReceiveBoomerang", 1f);
        anim.SetTrigger("Shot");
        modelBoomerang.rotate = false;
        modelBoomerang.gameObject.SetActive(false);
        aimIndicator.SetActive(false);
    }
    
    private void CanReceiveBoomerang()
    {
        canGetBoomerang = true;
    }

    #endregion

    #region PowerUps

    public void ActivateShield()
    {
        CancelInvoke("DeactiveShield");
        shield.SetActive(true);
        hasShield = true;
        Invoke("DeactiveShield", 5f);
    }

    void DeactiveShield()
    {
        hasShield = false;
        shield.SetActive(false);
    }

    public void ActivateBoost()
    {
        CancelInvoke("DeactiveBoost");
        fireTrail.SetActive(true);
        carController.m_FullTorqueOverAllWheels = 10000f;
        carController.m_Topspeed = 200f;
        carController.m_Downforce = 10000f;
        hasBoost = true;
        Invoke("DeactiveBoost", 5f);
    }

    void DeactiveBoost()
    {
        carController.m_FullTorqueOverAllWheels = tmpTorque;
        carController.m_Downforce = tmpDownForce;
        carController.m_Topspeed = tmpTopSpeed;
        hasBoost = false;
        fireTrail.SetActive(false);
    }

    #endregion
    
}
