﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarParticle : MonoBehaviour
{
    public ParticleSystem dust;
    public float revsDirtThreshold;
    public GameObject hit;
    
    private CarController carController;

    private void Awake()
    {
        carController = GetComponent<CarController>();
    }

    void Update()
    {
        if (revsDirtThreshold < carController.Revs)
        {
            if (!dust.isPlaying)
            {
                dust.Play();
            }
        }
        else
        {
            if (dust.isPlaying)
            {
                dust.Stop();
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        Instantiate(hit, other.GetContact(0).point, Quaternion.identity);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            Instantiate(hit, other.transform.position, Quaternion.identity);
        }
    }
}
